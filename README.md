# wheelman

Like [WheelDecide](https://wheeldecide.com/), but with more ELM and less horrible UX.

This is just a simple, small-scope project I did to try out ELM. You can see it in action on my site [here](https://wheelman.tremblay.dev/).
