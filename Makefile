main.js:
	elm make src/Main.elm --output=main.js --optimize

deploy: main.js
	rsync -e "ssh -p 22" --archive --delete bootstrap.css wheelstyle.css main.js index.html root@tremblay.dev:/usr/share/nginx/html/wheelman

clean:
	rm -f main.js
