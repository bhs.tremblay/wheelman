module Main exposing (main)

import Basics.Extra exposing (fractionalModBy)
import Browser
import Delay
import Html exposing (Html, button, div, text, textarea)
import Html.Attributes
import Html.Events
import List
import Random
import Svg exposing (Svg, svg)
import Svg.Attributes



-- CONFIGURABLE CONSTANTS


defaultColour : String
defaultColour =
    "#EC7063"


colours : List String
colours =
    [ defaultColour
    , "#A569BD"
    , "#5DADE2"
    , "#45B39D"
    , "#F5B041"
    , "#DC7633"
    , "#CD6155"
    , "#AF7AC5"
    , "#5499C7"
    , "#48C9B0"
    , "#52BE80"
    , "#F4D03F"
    , "#EB984E"
    ]


wheelRadius : number
wheelRadius =
    250


extraRotations : number
extraRotations =
    3


wheelCenter : Point
wheelCenter =
    Point 250 250


caretBase : number
caretBase =
    10


caretHeight : number
caretHeight =
    20



-- ACTUAL CONSTANTS


tau : Float
tau =
    2 * pi


caretPoints : String
caretPoints =
    let
        upperBase =
            Point
                (wheelCenter.x + wheelRadius)
                (wheelCenter.y - (caretBase / 2))

        lowerBase =
            Point
                (wheelCenter.x + wheelRadius)
                (wheelCenter.y + (caretBase / 2))

        tip =
            Point
                (wheelCenter.x + wheelRadius - caretHeight)
                wheelCenter.y
    in
    unparsePoint upperBase
        ++ " "
        ++ unparsePoint lowerBase
        ++ " "
        ++ unparsePoint tip
        ++ " "
        ++ unparsePoint upperBase


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- DATA STRUCTRES


type alias Point =
    { x : Float
    , y : Float
    }


type alias Wedge =
    { text : String
    , startRadian : Float
    , widthRadian : Float
    , colour : String
    }


type alias Model =
    { input : String
    , wedges : List Wedge
    , rotation : Float
    , spun : Bool
    , spinning : Bool
    , single : Bool
    }


defaultWedges : List Wedge
defaultWedges =
    [ Wedge "" 0 tau defaultColour ]


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model "" defaultWedges 0 False False True
    , Cmd.none
    )



-- UPDATE


type Msg
    = Input String
    | Spin
    | Rotation Float
    | Spun


enum : List a -> List ( Int, a )
enum list =
    List.indexedMap Tuple.pair list


rotate : Random.Generator Float
rotate =
    Random.float 0 tau


spin : Cmd Msg
spin =
    Random.generate Rotation rotate


isNotEmpty : String -> Bool
isNotEmpty string =
    not (String.isEmpty string)


parseInput : String -> List String
parseInput input =
    List.filter isNotEmpty (String.split "\n" input)


unparsePoint : Point -> String
unparsePoint point =
    String.fromFloat point.x ++ " " ++ String.fromFloat point.y


getWedgeColour : Int -> String
getWedgeColour index_of_wedge =
    let
        index_of_colour =
            modBy (List.length colours) index_of_wedge

        colour =
            List.head (List.drop index_of_colour colours)
    in
    case colour of
        Just c ->
            c

        Nothing ->
            defaultColour


buildWedge : Float -> ( Int, String ) -> Wedge
buildWedge widthRadian ( wedgeIndex, wedgeText ) =
    let
        startRadian =
            widthRadian * toFloat wedgeIndex

        wedgeColour =
            getWedgeColour wedgeIndex
    in
    Wedge wedgeText startRadian widthRadian wedgeColour


getWedges : String -> List Wedge
getWedges input =
    let
        inputs =
            parseInput input

        enumeratedInputs =
            enum inputs

        widthRadian =
            tau / toFloat (List.length inputs)
    in
    List.map (buildWedge widthRadian) enumeratedInputs


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Input input ->
            let
                wedges =
                    if input == "" then defaultWedges else getWedges input
            in
            ( { model
                | input = input
                , wedges = wedges
                , single = List.length wedges < 2
              }
            , Cmd.none
            )

        Spin ->
            ( model
            , spin
            )

        Rotation radians ->
            ( { model
                | rotation = model.rotation + (extraRotations * tau) + radians
                , spinning = True
              }
            , Delay.after 5 Delay.Second Spun
            )

        Spun ->
            ( { model
                | spun = True
                , spinning = False
              }
            , Cmd.none
            )



-- VIEW


wheelCoordinates : Float -> Float -> Point
wheelCoordinates radius theta =
    Point
        ((radius * cos theta) + wheelCenter.x)
        ((radius * sin theta) + wheelCenter.y)


wheelEdgeCoordinates : Float -> Point
wheelEdgeCoordinates theta =
    wheelCoordinates wheelRadius theta


viewSingleWedge : Wedge -> Svg msg
viewSingleWedge wedge =
    Svg.svg
        []
        [ Svg.circle
            [ Svg.Attributes.cx (String.fromFloat wheelCenter.x)
            , Svg.Attributes.cy (String.fromFloat wheelCenter.y)
            , Svg.Attributes.r (String.fromFloat wheelRadius)
            , Svg.Attributes.stroke "black"
            , Svg.Attributes.fill wedge.colour
            ]
            []
        , Svg.text_
            [ Svg.Attributes.x (String.fromFloat wheelCenter.x)
            , Svg.Attributes.y (String.fromFloat wheelCenter.y)
            , Svg.Attributes.textAnchor "middle"
            ]
            [ Svg.text wedge.text ]
        ]


viewWedge : Wedge -> Svg msg
viewWedge wedge =
    let
        wedgeArcStartPoint =
            wheelEdgeCoordinates wedge.startRadian

        wedgeArcEndPoint =
            wheelEdgeCoordinates (wedge.startRadian + wedge.widthRadian)

        wedgePath =
            "M "
                ++ unparsePoint wheelCenter
                ++ " L "
                ++ unparsePoint wedgeArcStartPoint
                ++ " A "
                ++ String.fromFloat wheelRadius
                ++ " "
                ++ String.fromFloat wheelRadius
                ++ " 0 0 1 "
                ++ unparsePoint wedgeArcEndPoint
                ++ " Z"
    in
    Svg.path
        [ Svg.Attributes.d wedgePath
        , Svg.Attributes.stroke "black"
        , Svg.Attributes.fill wedge.colour
        ]
        []


viewWedgeText : Wedge -> Svg msg
viewWedgeText wedge =
    let
        wedgeTextRadians =
            wedge.startRadian + (wedge.widthRadian / 2)

        wedgeTextPoint =
            wheelCoordinates (wheelRadius / 2) wedgeTextRadians

        -- SVG transform isn't working with radians so just use degrees
        wedgeTextDegrees =
            wedgeTextRadians * 180 / pi

        wedgeTextTransform =
            "rotate(" ++ String.fromFloat wedgeTextDegrees ++ " " ++ unparsePoint wedgeTextPoint ++ ")"
    in
    Svg.text_
        [ Svg.Attributes.x (String.fromFloat wedgeTextPoint.x)
        , Svg.Attributes.y (String.fromFloat wedgeTextPoint.y)
        , Svg.Attributes.transform wedgeTextTransform
        , Svg.Attributes.textAnchor "middle"
        ]
        [ Svg.text wedge.text ]


viewWheel : Model -> List (Svg msg)
viewWheel model =
    if model.single then
        let
            wedge =
                List.head model.wedges
        in
        case wedge of
            Just w ->
                [ viewSingleWedge w ]

            Nothing ->
                []

    else
        List.map viewWedge model.wedges
            ++ List.map viewWedgeText model.wedges


viewCaret : Svg msg
viewCaret =
    Svg.polygon
        [ Svg.Attributes.points caretPoints
        , Svg.Attributes.stroke "black"
        , Svg.Attributes.fill "red"
        , Svg.Attributes.strokeWidth "1.5"
        ]
        []


isResult : Float -> Wedge -> Bool
isResult rotation wedge =
    let
        pointerRadians =
            fractionalModBy tau -rotation

        wedgeEndRadians =
            wedge.startRadian + wedge.widthRadian
    in
    pointerRadians >= wedge.startRadian && pointerRadians < wedgeEndRadians


getResult : Model -> String
getResult model =
    let
        results =
            List.filter (isResult model.rotation) model.wedges

        result =
            List.head results
    in
    case result of
        Just wedge ->
            wedge.text ++ "!"

        Nothing ->
            "I don't know!"


view : Model -> Html Msg
view model =
    div [ Html.Attributes.id "wheelman" ]
        [ viewBanner model
        , viewMain model
        ]


viewButton : Model -> Html Msg
viewButton model =
    div
        [ Html.Attributes.class "col-md-4 text-center"
        , Html.Attributes.id "wheelman-spin"
        ]
        [ button
            [ Html.Attributes.id "wheelman-spin-button"
            , Html.Attributes.class "btn btn-dark btn-block"
            , Html.Attributes.disabled model.spinning
            , Html.Events.onClick Spin
            ]
            [ Html.text "Spin" ]
        ]


viewBanner : Model -> Html Msg
viewBanner model =
    div
        [ Html.Attributes.id "wheelman-banner"
        , Html.Attributes.class "row"
        ]
        [ viewResult model
        , viewButton model
        ]


viewMain : Model -> Html Msg
viewMain model =
    div
        [ Html.Attributes.id "wheelman-main"
        , Html.Attributes.class "row"
        ]
        [ viewCanvas model
        , viewInput model
        ]


viewCanvas : Model -> Html Msg
viewCanvas model =
    div
        [ Html.Attributes.class "col-md-8 text-center" ]
        [ viewSvg model ]


viewSvg : Model -> Html Msg
viewSvg model =
    Svg.svg
        [ Svg.Attributes.width "500"
        , Svg.Attributes.height "500"
        , Svg.Attributes.viewBox "0 0 500 500"
        ]
        [ Svg.g
            [ Html.Attributes.id "wheelman-wheel"
            , Html.Attributes.style "-webkit-transition" "all 5s cubic-bezier(0.25, 0.1, 0.25, 1)"
            , Html.Attributes.style "-webkit-transform" ("rotateZ(" ++ String.fromFloat model.rotation ++ "rad)")
            , Html.Attributes.style "transform-origin" "center"
            , Html.Attributes.style "pointer-events" "none"
            , Html.Attributes.style "z-index" "0"
            ]
            (viewWheel model)
        , Svg.g
            [ Html.Attributes.id "wheelman-caret"
            , Html.Attributes.style "z-index" "1"
            ]
            [ viewCaret ]
        ]


viewInput : Model -> Html Msg
viewInput model =
    div
        [ Html.Attributes.class "col-md-4"
        , Html.Attributes.id "wheelman-input"
        ]
        [ viewTextArea model ]


viewTextArea : Model -> Html Msg
viewTextArea model =
    Html.textarea
        [ Html.Attributes.id "wheelman-textarea"
        , Html.Attributes.placeholder "Add choices here..."
        , Html.Attributes.value model.input
        , Html.Events.onInput Input
        ]
        []


resultVisibility : Model -> String
resultVisibility model =
    if model.spun && not model.spinning then
        "visible"

    else
        "hidden"


viewResult : Model -> Html Msg
viewResult model =
    let
        text = getResult model
    in
    div
        [ Html.Attributes.id "wheelman-result"
        , Html.Attributes.class "col-md-8 text-center"
        , Html.Attributes.style "visibility" (resultVisibility model)
        ]
        [ Html.h2 [] [ Html.text text ] ]
